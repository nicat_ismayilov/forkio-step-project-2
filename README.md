This project was created and developed by Nicat Ismayilov and Rufat Babayev.

- the `Header` section(including part with Laptop image and "`Download For Free Now`" button) and `People are Talking About the Fork` section were created by ***Rufat Babayev***.
- Sections - `Revolutionary Editor`, `Here is what you get` and `Fork Subscription Pricing` were created by ***Nicat Ismayilov***.